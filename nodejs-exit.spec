%{?nodejs_find_provides_and_requires}
%global enable_tests 0
Name:                nodejs-exit
Version:             0.1.2
Release:             2
Summary:             A process.exit alternative that ensures STDIO are fully drained before exiting
License:             MIT
URL:                 https://github.com/cowboy/node-exit
Source0:             http://registry.npmjs.org/exit/-/exit-%{version}.tgz
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       npm(grunt-cli) npm(grunt-contrib-nodeunit) npm(grunt-contrib-watch) npm(which)
%endif
%description
%{summary}.

%prep
%setup -q -n package

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/exit
cp -pr package.json lib/ \
    %{buildroot}%{nodejs_sitelib}/exit
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
%nodejs_symlink_deps --check
/usr/bin/grunt nodeunit -v
%endif

%files
%doc LICENSE-MIT README.md
%{nodejs_sitelib}/exit

%changelog
* Thu Feb 29 2024 doupengda <doupengda@loongson.cn> - 0.1.2-2
- Optimizing the Writing of ExclusiveArch

* Thu Aug 20 2020 Anan Fu <fuanan3@huawei.com> - 0.1.2-1
- package init
